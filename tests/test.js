const {Builder, By, until} = require('selenium-webdriver');
const chrome = require('selenium-webdriver/chrome');

// const assert = require('assert');
const assert = require('chai').assert;

const XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;

// Some tests need to access the MINERVA proxy to, e.g., check which elements are highlighted. However, the tests
// do not run in the same scope as the plugin and thus they do not have access to the Proxy. Therefore, the plugin
// exposes the proxy by attaching it as a data attribute to the main div element.
const minervaProxyContainerClass = 'map-exploration-container';
const minervaProxyCode = `$('.${minervaProxyContainerClass}').data('minervaProxy')`;


function minervaLogin() {

    const xhr = new XMLHttpRequest();

    return new Promise(function (resolve, reject) {

        xhr.onreadystatechange = function () {

            if (xhr.readyState !== 4) return;

            if (xhr.status >= 200 && xhr.status < 300) {
                resolve(xhr);
            } else {
                reject({
                    status: xhr.status,
                    statusText: xhr.statusText
                });
            }
        };

        xhr.open("POST", 'http://localhost:8080/minerva/api/doLogin', false);
        xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        xhr.send("login=admin&password=admin");
    });
}

async function getRequest(uri) {

    const xhr = new XMLHttpRequest();

    return new Promise(function (resolve, reject) {

        xhr.onreadystatechange = function () {

            if (xhr.readyState !== 4) return;

            if (xhr.status >= 200 && xhr.status < 300) {
                resolve(xhr);
            } else {
                reject({
                    status: xhr.status,
                    statusText: xhr.statusText
                });
            }
        };

        xhr.open("GET", uri);
        xhr.send();
    });
}

describe('Exploration plugin', async function() {

    //Some functions can take a lot of time as they need, for isntance, start MINERVA interface
    this.timeout(20000);

    let driver;
    let minervaProxy;
    let pluginContainer;

    function wait(timeInMs) {
        return driver.executeAsyncScript(`
            const callback = arguments[arguments.length - 1];
            setTimeout(()=>callback(), ${timeInMs})`);
    }

    function deHighlightAll(){
        return driver.executeScript(`minervaProxy = ${minervaProxyCode}; minervaProxy.project.map.getHighlightedBioEntities().then( highlighted => minervaProxy.project.map.hideBioEntity(highlighted) )`);
    }

    async function countHighlighted(){
        return driver.executeAsyncScript(`
                    var callback = arguments[arguments.length - 1];
                    ${minervaProxyCode}.project.map.getHighlightedBioEntities().then(highlighted => callback(highlighted));                    
                `);
    }

    before(async function startWebDriver() {
        const opts = new chrome.Options().addArguments('--no-sandbox', '--headless', '--remote-debugging-port=9222');
        driver = await new Builder().setChromeOptions(opts).forBrowser('chrome').build();
        // driver = await new Builder().forBrowser('chrome').build();

        await driver.manage().window().maximize();

        const loginResponse = await minervaLogin();
        const minervaToken = JSON.parse(loginResponse.responseText).token;

        await driver.get('http://localhost:8080');
        await driver.manage().addCookie({name: 'MINERVA_AUTH_TOKEN', value: minervaToken});
        const pluginsResponse = await getRequest('http://localhost:8080/minerva/api/plugins/');
        const pluginHash = JSON.parse(pluginsResponse.responseText)[0].hash;
        // const pluginHash = '92e159374ef920e80d15f0d9406e61ab';

        await driver.get(`http://localhost:8080/minerva/index.xhtml?id=single-map&plugins=${pluginHash}`);

        pluginContainer = await driver.wait(until.elementLocated(By.css(`.${minervaProxyContainerClass}`)),10000);
        minervaProxy = await driver.executeScript(`return $('.${minervaProxyContainerClass}').data('minervaProxy')`);
    });

    after(async function finishWebDriver() {
        await driver.quit();
    });

    async function fillHistamine(){
        // Filling the query string into the search box is a workaround because it is highly non-trivial to
        // programatically click at a specific position on the map (which is a canvas). If this was possible we would instead
        // click on the position of a histamine element. Luckily, the effect should be the same as searching for
        // histamine. However, it means there is a functionality which is not tested (whether clicking on a element
        // triggers the appropriate response in the plugin).

        const searchInput = driver.findElement(By.css('.minerva-generic-search-panel .searchPanel input[name="searchInput"]'));
        const searchSubmit = driver.findElement(By.css('.minerva-generic-search-panel .searchPanel .minerva-search-button'));

        await searchInput.sendKeys('Histamine');
        searchSubmit.click();

        return driver.wait(until.elementLocated(By.css(`.minerva-generic-search-panel tbody div`)),5000);
    }

    describe("Data loading", function () {

        function registerD3Click(){
            //For some reason, jquery click method (and alsow WebElement's click method) does not work for
            // d3 elements. Registering a d3Click and calling d3Click instead of click works
            // another solution is to call HTML: $(`.ceSvg .ce-node circle`)[0].dispatchEvent(new Event('click'));
            driver.executeScript(`
                $.fn.d3Click = function () {
                    this.each(function (i, e) {
                    var evt = new MouseEvent("click");
                    e.dispatchEvent(evt);
                    });
                };
            `)
        }

        async function d3Click(jQuerySelect){
            //options are mouseEventInit options (https://developer.mozilla.org/en-US/docs/Web/API/MouseEvent/MouseEvent)
            return driver.executeScript(`${jQuerySelect}.d3Click()`);
        }

        function d3ClickContext(jQuerySelect){
            return driver.executeScript(`${jQuerySelect}[0].dispatchEvent(new CustomEvent('contextmenu'))`);
        }

        function mouseHover(jQuerySelect) {
            return driver.executeScript(`
                    ${jQuerySelect}[0].dispatchEvent(new Event('mouseover'));
            `);
        }

        before(async function() {
            registerD3Click();
            await fillHistamine();
        });


        it('should enable explore button', async function () {
            const classes = await driver.findElement(By.css('.ceGoButton')).getAttribute('class');
            assert(classes.indexOf('disabled') === -1, true);
        });

        async function startExploration(){
            driver.findElement(By.css('.ceGoButton')).click();
            await wait(500);
            return driver.wait(until.elementLocated(By.css(`.ceSvg .ce-node text`)), 1000);
        }

        describe('explore with chemicals only', function () {
            before(async function () {
                driver.executeScript("$('select.ceSelSpeciesType').val('Chemical')");
                await wait(100);
                await startExploration();
                await d3Click('$(".ceSvg .ce-node circle")');
                await driver.wait(until.elementLocated(By.css(`.ce-link`)),1000);
            });

            it("should have appropriate number of links and nodes", async function() {
                const links = await driver.findElements(By.css('.ce-link'));
                const nodes = await driver.findElements(By.css('.ce-node'));

                assert.equal(links.length, 10);
                assert.equal(nodes.length, 11);
            });

            after(async function () {
                await driver.executeScript("$('select.ceSelSpeciesType').val(['Complex', 'Gene', 'Protein', 'RNA'])");
                await wait(500);
            });
        });
        
        describe('click explore button', function() {

            before(async function () {
                await startExploration();
            });

            it('should create a single node', async function () {

                const node = driver.findElement(By.css(`.ceSvg .ce-node text`));
                assert.equal(await node.getText(), 'histamine');
            });

            describe("click restart exploration", function () {
                it("should clean the svg", async function () {
                    let nodes = await driver.findElements(By.css('.ce-node'));
                    assert.isAbove(nodes.length, 0);
                    driver.findElement(By.css('.ceRestartButton')).click();
                    await wait(500);

                    nodes = await driver.findElements(By.css('.ce-node'));
                    assert.equal(nodes.length, 0);
                });

                after(async function () {
                    await startExploration();
                })
            });

            describe("click histamine node", function () {

                before(async function() {
                    await d3Click('$(".ceSvg .ce-node circle")');
                    await driver.wait(until.elementLocated(By.css(`.ce-link`)),1000);
                });

                it("should have appropriate number of   links and nodes", async function() {
                    const links = await driver.findElements(By.css('.ce-link'));
                    const nodes = await driver.findElements(By.css('.ce-node'));

                    assert.equal(links.length, 11);
                    assert.equal(nodes.length, 12);
                });

                it("should 23 highlighted elements", async function () {
                    const highlighted = await countHighlighted();
                    assert.equal(highlighted.length, 23);
                });

                describe("hover click", function () {

                    let btnLocator;
                    before(async function () {
                        btnLocator = driver.findElement(By.css('button.ceLocatorButton'))
                        btnLocator.click();
                        await wait(200);
                    });

                    it("should activate locator button", async function () {
                        assert.include(await btnLocator.getAttribute("class"), "active");
                    });

                    it("should change position on hover", async function () {
                        const url = await driver.getCurrentUrl();
                        await mouseHover(`$('.ce-node text:contains("cortex")').parent()`);
                        await wait(500);
                        const urlNew = await driver.getCurrentUrl();
                        assert.notEqual(url, urlNew)
                    });

                    after(async function () {
                        btnLocator.click();
                        await wait(200);
                    });

                });

                describe("click HDC", async function () {
                    before(async function () {
                        await d3Click("$('.ce-node .ce-species:contains(HDC)').parent().find('circle')");
                        await driver.wait(until.elementLocated(By.css('.ce-link[marker-end*=ceMarkerArrowReactant]')),1000);
                    });

                    it("should give one more HDC record", async function () {
                        // we need to use by.js as Selenium does not handle "contains" when called with By.css
                        const hdcs = await driver.findElements(By.js(() => $('.ce-species:contains(HDC)')));
                        assert.equal(hdcs.length, 2);
                    });

                    it("should have exactly one reactant image", async function () {
                        const links = await driver.findElements(By.css('.ce-link[marker-end*=ceMarkerArrowReactant]'));
                        assert.equal(links.length, 1);
                    });

                    describe("focus on hdc", function () {
                        beforeEach(async function () {
                            await d3ClickContext("$('.ce-node .ce-species:contains(HDC)').parent().find('circle')")
                        });
                        
                        it("should show context menu", async function () {
                            const cMenu = await driver.wait(until.elementLocated(By.css('.ceNodeMenu')),1000);
                            assert.notInclude(await cMenu.getAttribute("class"), "hidden");
                        });

                        describe("click on locate", function () {
                            before(async function () {
                                await d3Click("$($('.ceNodeMenu div:contains(LOCATE)')[0])")
                                await wait(500);
                            });

                            it("should locate hdc in map", async function() {
                                const url = await driver.getCurrentUrl()                               ;

                                assert.include(url, 'x=1486');
                                assert.include(url, 'y=710');
                            })
                        });

                        describe("backtrack", function () {
                            before(async function () {
                                await d3Click("$('.ceNodeMenu div:contains(BACKTRACK)')")
                                await driver.executeAsyncScript(function () {
                                    const callback = arguments[arguments.length - 1];
                                    setTimeout(()=>callback(), 500);
                                })
                            });

                            it("should highlight the path", async function() {
                                const links = await driver.findElements(By.css('.ce-path-link'));
                                assert.isAbove(links.length, 1);
                            })

                        })
                    })
                });
            })
        })
    });
});