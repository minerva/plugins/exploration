# Map exploration plugin

Plugin enabling to explore a map in a more single-gene-centric and focused way.

Find detailed user documentation <a href="docs">here</a>.

To compile the plugin run:

- ```npm install```
- ```npm run build```

After that, you should see ```plugin.js``` file in the ```dist``` directory. 
This file needs to be uploaded to a place where your Minerva instance can see it.
Then you simply use the URI of the ```plugin.js``` or ```plugin_chemicals.js``` inside the Minerva's plugin
interface to load it.