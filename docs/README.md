# Mineva exploration plugin

The goal of the *exploration plugin* for the <a href="http://r3lab.uni.lu/web/minerva-website/">Minerva framework</a>
(engine on which runs, e.g., <a href="http://minerva.uni.lu/MapViewer/" target="_blank">Parkinson's disease map</a> 
or <a href="https://vmh.uni.lu/#reconmap" target="_blank">Recon map</a>)
is to provide a more focused way of molecular interaction network exploration then a simple searching,
zooming and panning, which is by default provided by Minerva. It finds use in situations when a user
wants to explore a neighborhood of a molecule appearing multiple times in a network. That is often the case
in large networks when certain entities need to be duplicated to obtain a more readable layout. Another
example is when visualizing disease networks where entities which are believed to be associated
with given disease are studied more extensively and thus appear in different compartments and contexts.
For example, the main neurpathological feature of Parkinson's
disease (PD) are Lewy bodies major component of which is Alpha synuclein (SNCA). Indeed, when searching
for SNCA in <a href="http://minerva.uni.lu/MapViewer/">PD MAP</a>, one finds more than 100 occurrences of this
gene in different compartments, involved in various roles in different processes (see the image below).
Exploring interaction partners of SNCA by panning and zooming is thus a daunting task.

![SNCA in PD](img/pd-snca-example.png)

The exploration plugin provides a convenient way of exploring interaction partners of an entity 
irrespective on its position in the map (or submap for that matter). With the exploration plugin, the user can:

- See in which (sub)maps an interacting partners of the EOI are present
- See in which cell compartments interacting partners of the EOI are present
- Iterative exploration of partners' neighborhoods in given compartment (localized exploration)
- Restrict types of interacting partners
- Highlight identified interactions in the map
- Locate interacting entities in the map
- Trace back list of interactions to the EOI
- Search for entities or reaction type by keyword
- Export the current state of the exploration

## Documentation

- [General instructions](#general-instructions)
- [Export](#export)
- [Graphical symbols](#graphical-symbols)
- [Settings](#settings)

### General instructions

In order to start the plugin, open the plugin menu in the MINERVA's upper left corner (see image below) and click plugins.
In the dialog which appears enter the following address in the URL box: 
`https://minerva-dev.lcsb.uni.lu/plugins/exploration/plugin.js` or
 `https://minerva-dev.lcsb.uni.lu/plugins/exploration/plugin_chemicals.js` (when you want the
 plugin by default also to explore chemicals).
The plugin shows up in the plugins panel on the right hand side of the screen.

![SNCA in PD](img/plugin_start.png)

#### Starting the exploration

To start the exploration, the user needs to select a bioentity neighborhood of which she wants to explore. 
We will call this bioentity *entity of interest (EOI)* (in the following text, we will use PRKN gene as the EOI).
This can be done either by **clicking on an instance** of it **in the map** or by **searching** for it **using the search box**.

The color of the *Explore* button in the top left corner of the plugin turns to dark green and
shows the name of the chosen bioentity when hovering over it.

Clicking on *Explore* creates a node representing the chosen entity on the canvas. 

Anytime one can **pan and zoom** in the visualization:

- Moving around the visualization is possibly by click and drag.
- Zooming in and out is possible as well.


#### Exploration

By **left clicking** the initial node, the plugin 
connects to the server and obtains all the **entities** which are **connected** to it by any reactions.
Please note that **only elements of the selected types** (see [Settings](#settings)) **are visible!**. 
The retrieved entities are then visualized as a tree by **grouping** them **first by (sub)map** 
and **second by compartment** they reside in. The (sub)map nodes are represented by gray diamond symbols 
and compartments by gray rectangles with rounded edges. 

The tree in this stage thus has four levels: 

0. The root representing all instances of the the EOI anywhere in the map (including submaps).
1. The (sub)maps in which the EOI connects by a reaction to any entity (of the selected type).
2. The compartments in given (sub)map in which the EOI connects by a reaction to any entity (of the selected type).
3. The entities (of the selected type) connected by a reaction to the EOI. The arrow head connecting the 
compartment node with the entity node specifies the type of relation of the two connected
entities (see [Graphical symbols](#graphical-symbols) for details). If given (sub)map and compartment contains
multiple times a pair of entities connected by the same type of reaction in the same role, 
this pair will be shown only once, but information about it can be obtained 
by hovering over the target (right hand side) node which reveals a tooltip showing all bioentities represented
by that particular node.

![PRKN](img/prkn1.png) 

By left **clicking** any of the **inner nodes**, the corresponding subtree gets **folded or unfolded** making more space
for the rest of the nodes.

![PRKN](img/prkn2.png)

By left **clicking** on a **leaf entity** node, the plugin connects again to the server and retrieves 
all **bioentities connected** to it by a reaction and shows them grouped by compartment. 

If there is no connected entity which has not been encountered yet, the node changes to empty node (no fill color).

Please note that here only neighbors connected to the bioentities represented by that specific node are shown. 
I.e., if a node represents a gene G in a compartment C only neighbors of gene G in C are shown irrespective 
whether G appears also in other compartment D. That is, **unlike** at the **root** level, the exploration is restricted
to a specific position in the map - **localized exploration**.

![PRKN](img/prkn3.png)

#### Recursive exploration

Context menu (right click) of any node provides also access to the *recursive exploration*
functionality which allows to **recursively explore neighbors of a node**, i.e.
after generating nodes for all neighbors of the starting node, the procedure 
repeats recursively for every of those neighbors. Since this can lead
to creation of thousands of nodes, which would significantly slow down the browser, 
the procedure stops after retrieving given number of nodes. This threshold
can be set in the *Setttings* (see bellow). Please note, that **a web browser
can without problems handle only several thousands of elements** and thus setting
the value of this threshold to a value higher then about 10.000 (depending 
on your hardware configuration) will probably have
a significant impact on the responsiveness of the whole application.

#### Spawning multiple trees 

If at some point the user is interested what are neighbors of an encountered bioentity anywhere in the map, 
i.e. go from local exploration to global exploration, she can right click on that node and choose *SPAWN NEW ROOT*. 
With this operation a new independent root is spawned and this root node again represent all instances of given 
bioentity anywhere in the map and submaps. Any node in the previous tree which
matches the new root is highlighted by thick black circle around that node.

The spawned trees are tracked by rectangular buttons in the upper part of the visualization. Clicking on 
any of those, positions the view so that it is centered on the root of the respective tree.

A tree can be removed by right clicking on the root of a node and selecting *Remove root*. This operation 
removes not only the respective tree, but also all the following trees since those were spawned from the tree
being removed which is not valid any more.

![PRKN](img/prkn4.png)

#### Searching

When the EOI appears often in the map, searching for an entity in the tree just by eyeballing might be difficult. 
Therefore, the exploration lugin offers search functionality. 
Using the two **text boxes in the lower part of the plugin**, the user
can either **search for entities** or **reaction types** containing entered keyword. 
The non-matched entities become transparent
thus highlighting the matched entities and the entities from the roots of the respective trees to the matched entities.
See below an example of searching for *PINK*. 

![prkn-search](img/prkn-search.png)

#### Interaction with the map

All bioentities represented by the nodes in the visualization are highlighted in the map. This also holds 
for all reactions which connect an entity to its predecessor in the exploration. 
 
Specifically, the EOI is highlighted by an icon while all other entities and reactions are highlighted 
by color overlay.
 
The highlighted parts of the map can be either explored by panning and zooming, but the user can also
**right click on any node** in an exploration tree and **select *LOCATE*** which takes her to the **corresponding
position in the map**. By choosing *LOCATE SUBTREE* item, which is available for non-leaf nodes, one can focus on a part
of the map covering all the nodes in the subtree rooted in the respective node.

The **locating function** can be **turned on** by default by clicking the **bulls eye icon** in the upper part of the plugin.
If turned on, then every time a user hovers over a node, the corresponding entities will be located in the map.


#### Backtracking

It might be difficult to see in the map the sequence of reactions which connect given bioentity with the EOI, 
especially when multiple roots were spawned. If that is the case, one can right click on any node and select 
*BACKTRACK*. This will highlight all paths from the selected node to the root of the first tree (if the backtrack
depth is not specified - see \[see [Settings](#settings)\]). The path will also be highlighted in the map, 
temporarily turning off other highlights created by the plugin. 

Backtracking will result in multiple paths in situations when the root of one tree corresponds to 
multiple bioentities in the previous tree.

The backtrack paths can be turned of by right clicking on the node from which the last backtrack was started  
and selecting *CLEAR BACKTRACK*.

![Backtrack](img/backtrack.png)

### Export

**All the currently visible elements** in the visualization **can be exported** either to a [CSV](https://en.wikipedia.org/wiki/Comma-separated_values) 
or [JSON](https://en.wikipedia.org/wiki/JSON) file by clicking the export button in the lower right corner of the plugin. 

Please note that the data are hierarchical and therefore
the JSON format is more suitable for storing the data. If there are multiple spawned trees, 
they are all exported and separated by an empty line in the resulting CSV. 

### Graphical symbols

To distinguish different types of information in the visualization, the exploration plugin uses different
graphical symbols for nodes and lines arrow heads.

#### Nodes

There are three types of nodes:

- *Circles* represent bioentities nodes and their color correspond to the exploration tree they appear in.
If the the sphere has no fill color, there are no neighbors to explore (all appear in the path from that node 
to the root of the respective tree).
- *Gray rectangles with rounded corners* represent compartment nodes
- *Gray diamonds* correspond to (sub)maps nodes

#### Lines

Lines differ by their arrow heads which represent what is the role of the two connected entities 
in a reaction. The arrow head can be either circle, arrow, or vertical bar depending on the reaction
type and role of the right hand side entity (target). The mapping is as follows:

- *circle*: the target modulates the reaction
- *vertical bar*: the reaction types is one of *Inhibition*, *Unknown inhibition*, *Negative influence*,
*Unknown negative influence*
- arrow: the remaining reaction types

The direction of the arrow/position of the vertical bar determines the reactant/product role of the target. 
If the arrow goes from left to right or the vertical bar is on the 
right hand side of the head, the target is a product and otherwise it is a reactant.
This does not apply to the circle symbol since modulation is not directional.

### Settings

The exploration plugin has several settings which can impact the exploration process. These settings
can be accessed by clicking on the settings button in the upper part of the plugin:

- *Species types*: the user can select which types of bioentities will be considered during the
exploration. Changing this does not impact the existing exploration state.
Thus, for this change to take effect, one needs to restart the exploration (either by clicking
on the restart or *Explore** button).
- *Locate on expansion*: if checked, when a node is expanded, the newly shown nodes are located in the map.
- *Max. elements in full expansion*: when full expansion is selected from a node's context menu, 
at most *Max. elements in full expansion* nodes will be retrieved and expansion will
be stopped after reaching this threshold
- *Backtrack depth*: If set, the backtracking procedure will go only given number of levels back.
- *Highlight by icon/surface*: sets the way of how encountered entities are highlighted in the map.
