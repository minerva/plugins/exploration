/* globals minervaDefine */

require('../css/styles.css');
require('../css/minerva.css');

const minervaWrapper = require('minerva-wrapper');
const MinervaElement = minervaWrapper.minerva.Element;
const Reaction = minervaWrapper.minerva.Element;


const d3 = require('d3');
// const $ = require('jquery');
// const d3 =  Object.assign({}, require('d3-scale'), require('d3-zoom'), require('d3-selection'), require('d3-hierarchy'), require('d3-collection'));
// const d3.event = require('d3-selection').event; //conflicts when using react, babel, webpack

const deepCopy = require('deep-copy');
const helper = require('./helper');

/*********************
 Initialization of constants and settings
 *********************/

const pluginName = 'Map exploration';
const pluginVersion = '1.1.0';
const minWidth = 400;

const containerName = 'map-exploration-container';

const PATH_EQUALITY_ATTRS = ["name", "_type", /*"role", "reactionType",*/ "_compartmentId", "_complexId"]; //To be used for filtering out already encountered species

const settings = {
  marginTop: 0,
  marginRight: 10,
  marginBottom: 0,
  marginLeft: 10,
  treeMarginRight: 10,
  nodeCircleSize: 10,
  nodeSpacingVertical: 2,
  d3TreeHeightCorrection: 0, //TODO - for some reason, the tree coordinates overflow given height when too many items are present -> decreasing height by a constant make the tree to fit the view
  treeLevelWidth: 130, //number of pixels taken by one level of the tree in the visualization
  highlightedNodeOpacity: 0.3,
  highlightedNodeBorder: 3,
  highlightedNodeBorderOpacity: 1,

  compartmentTypeName: "compartment",
  emptyCompartmentName: "Extracellular",
  compartmentSymbolColor: "gray",

  submapTypeName: "(sub)map",

  bioEntityTypeMapping: {
    'Ion': 'Chemical',
    'Simple molecule': 'Chemical',
    'Drug': 'Chemical'
  },

  defaultSpecies: ["Protein", "Complex", "Gene", "RNA"],

  modelComparmentTextOffsetY: "1em",

  optSpeciesTypesListSize: 10,

  markerPaddingRight: 2,
  markerColor: "#ccc",

  helpPage: 'https://git-r3lab.uni.lu/minerva/plugins/exploration/tree/master/docs',

  neighboursBySurface: true, //If true, encountered elements are highlighted by area. If false, by icon.
  maxFullExapnd: 1000,
  maxNodeNameLength: 15,
  duration: 500
};

// settings.height = $(window).height() -  - settings.marginTop - settings.marginBottom;
settings.nodeTextOffsetX = settings.nodeCircleSize + 10;

settings.markerWidth = Math.min(50, settings.treeLevelWidth / 2); //TODO test this
settings.markerHeight = settings.nodeCircleSize;


const globals = {
  treeRoots: [],
  cnt_nodes: 0
};

// ******************************************************************************
// ********************* PLUGIN REGISTRATION WITH MINERVA *********************
// ******************************************************************************

let pluginContainer;
let pluginContainerId;
var minervaVersion;

const register = function () {
  // window.minervaInstance = _minerva;
  $(".tab-content").css('position', 'relative');

  pluginContainer = $(minervaWrapper.getElement());
  pluginContainerId = pluginContainer.attr('id');
  if (!pluginContainerId) {
    //the structure of plugin was changed at some point and additional div was added which is the container but does not have any properties (id or height)
    pluginContainer.css('height', '100%');
    pluginContainerId = pluginContainer.parent().attr('id');
  }
  pluginContainer.addClass(containerName);
  helper.log("registering exploration plugin");
  return initPlugin();
};

const unregister = function () {
  helper.log("unregistering CE plugin");
  unregisterListeners();
  return deHighlightAll();
};

minervaWrapper.init({
  register: register,
  unregister: unregister,
  name: pluginName,
  version: pluginVersion,
  minWidth: minWidth,
  pluginUrl: 'https://minerva-service.lcsb.uni.lu/plugins/exploration/plugin.js',
});

function initPlugin() {

  minervaWrapper.addMapListener({
    dbOverlayName: "search",
    type: "onSearch",
    callback: searchListener
  });

  minervaWrapper.addMapListener({
    object: "plugin",
    type: "onResize",
    callback: pluginResize
  });

  return minervaWrapper.getVersion().then(function (version) {
    minervaVersion = parseFloat(version.split('.').slice(0, 2).join('.'));
    if (minervaVersion > 12.2) {
      minervaWrapper.addMapListener({
        dbOverlayName: "search",
        type: "onFocus",
        callback: searchListener
      });
    }
  }).then(function () {
    initMainPageStructure();
    initSvgStructure();
    return initGlobals().then(() => {
      pluginContainer.data("minervaWrapper", minervaWrapper);
    });
  });
}

function unregisterListeners() {
  minervaWrapper.removeAllListeners();
}

// ****************************************************************************
// ********************* Page structure and interaction *********************
// ****************************************************************************

function initMainPageStructure() {

  // The following functions are required since starting MINERVA v.14 bootstrap was upgraded to version >4
  const createExpansionInputGroup = function () {
    if (minervaVersion < 14) {
      return `
                <div class="input-group ceFullExpansionMaxCountGroup" title="Maximum number of nodes to be retrieved during full expension (please note that showing more than few thousand of nodes can cause significant slowdown of the broser).">
                    <input type="number" name="ceFullExpansionMaxCount" class="form-control" min="1" value=${settings.maxFullExapnd}>
                    <span class="input-group-addon">Max. elements in recursive exploration</span>
                </div>`;
    } else {
      return `
                <div class="input-group ceFullExpansionMaxCountGroup" title="Maximum number of nodes to be retrieved during full expension (please note that showing more than few thousand of nodes can cause significant slowdown of the broser).">
                    <input type="number" name="ceFullExpansionMaxCount" class="form-control" min="1" value=${settings.maxFullExapnd}>
                    <div class="input-group-append">
                        <span class="input-group-text">Max. elements in recursive exploration</span>
                    </div>                    
                </div>`;
    }
  };

  const createBacktrackDepthInputGroup = function () {
    if (minervaVersion < 14) {
      return `
                 <div class="input-group">
                    <input type="number" id="ceBacktrackMaxDepth" class="form-control" min="1">
                    <span class="input-group-addon">Backtrackdepth</span>
                 </div>`;
    } else {
      return `
                <div class="input-group">
                    <input type="number" id="ceBacktrackMaxDepth" class="form-control" min="1">
                    <div class="input-group-append">
                        <span class="input-group-text">Backtrackdepth</span>
                    </div>
                 </div>`;
    }
  };

  const createHighlightGroup = function () {
    if (minervaVersion < 14) {
      return `
                 <div class="form-inline">
                    <label class="radio-inline">
                        <input type="radio" name="inlineRadioOptions" class="ce-highlightRadioIcon"> Highlight by icon
                    </label>
                    <label class="radio-inline">
                        <input type="radio" name="inlineRadioOptions" class="ce-highlightRadioSurface"> Highlight by surface
                    </label>
                 </div>`;
    } else {
      return `
                <div class="form-check">
                    <div class="form-check form-check-inline">
                        <input class="form-check-input ce-highlightRadioIcon" type="radio" name="inlineRadioOptions" value="option1">
                        <label class="form-check-label">Highlight by icon</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input ce-highlightRadioSurface" type="radio" name="inlineRadioOptions" value="option1">
                        <label class="form-check-label">Highlight by surface</label>
                    </div>
                </div>`;
    }
  };

  pluginContainer.html(
    `
        <div class="modal fade" id="ceModal" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content"></div>
            </div>
        </div>
        <div class="container-fluid" >
            <div class="ceRowOptions row">
                <div class="col-md-4">
                    <button type="button" class="ceGoButton btn btn-block btn-sm btn-success disabled" title="Start exploration" style="width:100%">Explore</button>
                </div>
                <div class="col-md-2">
                    <button type="button" class="ceLocatorButton btn btn-block btn-sm btn-primary" title="Turn on/off map focus by hovering"><span class="fa fa-bullseye"></span>&nbsp;</button>
                </div>
                <div class="col-md-2">
                    <button type="button" class="ceRestartButton btn btn-block btn-sm btn-primary" title="Restart exploration"><span class="fa fa-repeat"></span>&nbsp;</button>
                </div>                
                <div class="ceRowOptionOptions col-md-2">
                    <button type="button" class="ceOptionsShowHideButton btn btn-block btn-sm btn-primary" title="Options"><span class="fa fa-cog"></span>&nbsp;</button>                    
                </div>
                <div class="ceRowOptionOptions col-md-2">
                    <button type="button" class="ceHelpButton btn btn-block btn-sm btn-primary" title="Help"><span class="fa fa-question-circle"></span>&nbsp;</button>                    
                </div>
            </div>
            <div class="row ceRowOptionOptions">
                <div class="col-md-12">
                    <form class="ce-hidden ceOptionsForm">
                        <div class="form-group">
                            <label class="label-species-types">Species types:
                                <select multiple class="ceSelSpeciesType form-control" size=${settings.optSpeciesTypesListSize}></select>
                            </label>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" id="ceLocateOnExpansion"> Locate on expansion
                                </label>
                            </div>
                            ${createExpansionInputGroup()}
                            ${createBacktrackDepthInputGroup()}
                        </div>
                        ${createHighlightGroup()}
                                
                    </form>                
                </div>            
            </div>
            <div class="row">
                <div class="col-md-12 ce-legend-row">
                    <div class="ceLegend"></div>
                </div>
            </div>
        </div>
        <div class="ceChart" style="position: relative; overflow: hidden;">
            <div class="ceTooltip hidden"></div>
            <div class="ceNodeMenu hidden"></div>
            <div class="cePathMenu hidden"></div>
            <!--<div class="ceMenu hidden"></div>            -->
        </div>
        <div class="ceSearchBarContainers container-fluid">
            <div class="row no-gutter">
                <div class="col-sm-12 col-md-5">
                    <div class="ce-search-bar-container">
                        <input type="text" class="ceInputSearchSpeciesName form-control"  placeholder="Species name" >
                    </div>
                </div>
                <div class="col-sm-12 col-md-5">
                    <div class="ce-search-bar-container">
                        <input type="text" class="ceInputSearchReactionType form-control"  placeholder="Reaction type" >
                    </div>
                </div>
                <div class="col-sm-6 col-md-1">
                    <div class="ce-search-bar-container">
                        <input type="text" class="form-control" >
                        <button class="btn btn-xs search" type="submit">
                            <span class="fa fa-search"></span>
                        </button>
                    </div>
                </div>
                <div class="col-sm-6 col-md-1">
                    <div class="ce-search-bar-container last">
                        <input type="text" class="form-control" >
                        <button class="btn btn-xs export" type="text">
                            <span class="fa fa-download"></span>
                        </button>
                    </div>
                </div>
            </div>
        </div>`
  );

  // pluginContainer.find('.ceRowOptions button').attr('data-toggle', 'tooltip');
  // pluginContainer.find('.ceRowOptions button').tooltip();

  recomputeChartDimensions();

  //bind events
  pluginContainer.find('.ceGoButton').on('click', () => {
    return go().catch(helper.errorHandler);
  });
  pluginContainer.find('.ceLocatorButton').on('click', () => locator());
  pluginContainer.find('.ceRestartButton').on('click', () => {
    return reinitializeVisualization().catch(helper.errorHandler);
  });
  pluginContainer.find('.ceHelpButton').on('click', () => window.open(settings.helpPage, '_blank'));
  pluginContainer.find('.ceOptionsShowHideButton').on('click', () => pluginContainer.find('.ceOptionsForm').toggle());
  // pluginContainer.find('.ceRowOptionOptions').on('mouseleave', () => pluginContainer.find('.ceOptionsForm').hide('fast'));
  pluginContainer.find('.ceSelSpeciesType').on('change', () => reinitializeVisualization());
  pluginContainer.find('.ce-highlightRadioIcon').on('click', () => setHighlightBySurface(false));
  pluginContainer.find('.ce-highlightRadioSurface').on('click', () => setHighlightBySurface(true));
  pluginContainer.find(".ce-search-bar-container button.search").on("click", () => searchFilterUpdate());
  pluginContainer.find(".ceInputSearchSpeciesName,.ceInputSearchReactionType").on("keydown", function (e) {
    if (e.keyCode === 13) searchFilterUpdate();
  });
  pluginContainer.find(".ce-search-bar-container button.export").on("click", () => exportData());
  if (settings.neighboursBySurface) pluginContainer.find(".ce-highlightRadioSurface").prop("checked", true);
  else pluginContainer.find(".ce-highlightRadioIcon").prop("checked", true);

  pluginContainer.find('.ceRowOptions button').mouseup(function () {
    $(this).blur()
  });

  pluginContainer.find('input[name="ceFullExpansionMaxCount"]').focusout(e => {
    const $target = $(e.target);
    if ($target.val() === undefined || $target.val() < 1) {
      $target.val(1);
    }
  });


  minervaWrapper.getConfiguration().elementTypes
    .map(t => getBETypeMapping(t.name))
    .sort()
    .forEach(function (t, i, arr) {
      if (i == 0 || (i > 0 && arr[i - 1] != arr[i]))
        pluginContainer.find(".ceSelSpeciesType").append('<option val="' + t + '">' + t + '</option>');
    });
  //By default check protein
  pluginContainer.find(".ceSelSpeciesType").val(settings.defaultSpecies);

  pluginContainer.find('button.headerHideButton').trigger('click'); //TODO - remove after interface to google maps resize available
}

function getBETypeMapping(beType) {
  return beType in settings.bioEntityTypeMapping ? settings.bioEntityTypeMapping[beType] : beType;
}

/**
 * Computes height of the chart region, which is the height of the page - height of the options part of the page.
 */
function recomputeChartDimensions() {
  helper.log("plugin row options' parent height", pluginContainer.find('.ceRowOptions').parent().height());
  pluginContainer.find('.ceChart').css('height', 'calc(100% - ' + pluginContainer.find('.ceRowOptions').parent().height() + 'px)');
}

/**
 * Called when Minerva sends event that plugin width was changed or can be hooked to global window resize event.
 */
function pluginResize() {
  pluginContainer.find('.ceSvgOverlayRect').css('width', pluginContainer.find('.ceSvgOverlayRect').closest('svg').width() + 'px');
}

const go = function () {
  pluginContainer.find('.ceOptionsForm').toggle(false);
  if (pluginContainer.find(".ceGoButton").attr("class").indexOf("disabled") < 0) {
    return addRoot(globals.searchElements);
  } else {
    return Promise.resolve();
  }
};

const locator = function () {

  let svg = pluginContainer.find(".ceChart").find("svg");
  if (svg.attr("class") === undefined || svg.attr("class").indexOf("ce-cursor-glass") < 0) {
    if (svg.attr("class") !== undefined)
      svg.attr("class", svg.attr("class") + " ce-cursor-glass");
    else
      svg.attr("class", "ce-cursor-glass");
    pluginContainer.find(".ceLocatorButton").addClass('active');

  } else {
    pluginContainer.find(".ceChart").find("svg").removeClass("ce-cursor-glass");
    svg.attr("class", svg.attr("class").replace("ce-cursor-glass", ""));
    pluginContainer.find(".ceLocatorButton").removeClass('active');
  }
};

const reinitializeVisualization = function () {
  return new Promise(function (resolve, reject) {
    try {
      removeRootsFrom(0);
      // pluginContainer.find(".ce-tree").remove();
      resolve();
    } catch (e) {
      reject(e);
    }
  }).then(function () {
    return highlightInMap();
  });
};

const setHighlightBySurface = function (state) {
  settings.neighboursBySurface = state; //true or false
};

const exportData = function () {
  openModal({
    body:
      '<button type="button" class="btn btn-block ceButtonExportCSV">CSV</button>' +
      '<button type="button" class="btn btn-block ceButtonExportJSON">JSON</button>'
  });

  pluginContainer.find('.ceButtonExportCSV').on('click', () => exportToFormat('csv'));
  pluginContainer.find('.ceButtonExportJSON').on('click', () => exportToFormat('json'));
};

function searchFilterUpdate() {
  let speciesName = pluginContainer.find(".ceInputSearchSpeciesName").val();
  let reactionType = pluginContainer.find(".ceInputSearchReactionType").val();

  globals.treeRoots.forEach(function (root) {
    root.eachAfter(function (node) {
      //if any of the children of this node needs is visible than this node needs to be shown as well
      if ("children" in node) {
        for (let i = 0; i < node.children.length; ++i) {
          if (node.children[i].show) {
            node.show = true;
            return;
          }
        }
      }
      if (isNotCompartmentNodeData(node.data)) {
        node.show = true;
        let bioEntity = node.data.bioEntityIds[0];
        if (!(
          (speciesName == '' || bioEntity.getName().toLowerCase().includes(speciesName.toLowerCase())) &&
          (reactionType == '' || !('reactionType' in bioEntity) ||
            bioEntity.reactionType.toLowerCase().includes(reactionType.toLowerCase()))
        )) {
          node.show = false;
        }
      } else {
        //If its compartment and was set to true because of its children than it needs to be hidden
        node.show = false;
      }
    })
  });

  helper.log("tree roots after application of filters", globals.treeRoots);
  globals.treeRoots.forEach(function (root, i) {
    updateLayout(root, i);
  });
}

function searchListener(elements) {
  globals.searchElements = elements;
  if (globals.searchElements.length > 0) { //empty when clicked on white space
    if (globals.searchElements[0].length !== undefined) globals.searchElements = globals.searchElements[0];

    if (globals.searchElements.length > 0 && globals.searchElements[0].constructor.name !== "Reaction") {
      pluginContainer.find(".ceGoButton").removeClass("disabled");
      pluginContainer.find(".ceGoButton").attr('title', 'Start exploration from ' + globals.searchElements[0].getName());
      return;
    }
  }
  pluginContainer.find(".ceGoButton").addClass("disabled");
  pluginContainer.find(".ceGoButton").attr('title', 'Start exploration');
}

function initSvgStructure() {
  globals.color20c = d3.scaleOrdinal(d3.schemeCategory10);
  globals.zoom = d3.zoom()
    .scaleExtent([0.1, 50])
    .on("zoom", zoomed);

  pluginContainer.find(".ceChart").append("<svg><defs></defs></svg>");

  const svg = d3.select("#" + pluginContainerId + " .ceChart svg")
    .attr("class", "ceSvg")
    .call(globals.zoom)
    .append("g")
    .attr("transform", "translate(" + settings.marginLeft + "," + settings.marginTop + ")");

  const rect = svg.append("rect")
    .attr("class", "ceSvgOverlayRect")
    .on("click", function (d) {
      pluginContainer.find(".ceNodeMenu").addClass("hidden");
      pluginContainer.find(".cePathMenu").addClass("hidden");

      // const menu = d3.select("#" + pluginContainerId + " .ceMenu");
      // menu.classed("hidden", true);
    });

  globals.svgG = svg.append("g");

  createMarkers();
}

/**
 * Makes passed identifier unique by adding plugin id to it. This needs to be done for
 * elements which need to be identified by id which would not be unique if the plugin
 * was used multiple times on the same page.
 * @param  id Identifier which needs to be uniquified.
 * @returns {*} Uniquified identifier.
 */
function uniquifyId(id) {
  return pluginContainerId + id;
}

function createMarkers() {
  // if ($("#ceMarkerArrowProduct").size()) {
  //     //markers were already created by other instance of the plugin
  //     return;
  // }
  // let nodeCircleOffset = getSvgClassStrokeWidth("ce-node") + settings.nodeCircleSize / 2;
  const linkStrokeWidth = getSvgClassStrokeWidth("ce-link");

  const circleR = settings.markerHeight * 0.95 / 2;
  const arrowWidth = settings.markerHeight;
  const dotR = settings.markerHeight * 0.1;
  const dotSpacing = settings.markerWidth / 2 / 4; //3 dots spanning half of the marker

  const markerVerticalLineHeight = settings.nodeCircleSize;
  const markerVerticalLineStrokeWidth = linkStrokeWidth * 2;

  const centerVertical = settings.markerHeight / 2;
  const centerHorizontal = settings.markerWidth / 2;

  const defs = d3.select("#" + pluginContainerId + " .ceChart svg defs");

  /* ARROW */
  var x1 = settings.markerWidth - settings.markerPaddingRight - settings.markerHeight, y1 = 0;
  var x2 = settings.markerWidth - settings.markerPaddingRight, y2 = centerVertical;
  var x3 = settings.markerWidth - settings.markerPaddingRight - settings.markerHeight, y3 = settings.markerHeight;
  var markerArrowProduct = defs.append('marker').attr("id", uniquifyId("ceMarkerArrowProduct"));
  markerArrowProduct.append("line")
    .attr("x1", 0)
    .attr("y1", function () {
      return y2;
    })
    .attr("x2", function () {
      return x1;
    })
    .attr("y2", function () {
      return y2;
    })
    .style("stroke", settings.markerColor)
    .style("stroke-width", linkStrokeWidth);
  markerArrowProduct.append("path")
    .attr("d",
      " M " + x1 + "," + y1 +
      " L " + x2 + "," + y2 +
      " L " + x3 + "," + y3 +
      " L " + x1 + "," + y1)
    .style("fill", settings.markerColor);

  /* BACK ARROW */
  var x1 = centerHorizontal, y1 = centerVertical;
  var x2 = centerHorizontal + arrowWidth, y2 = 0;
  var x3 = centerHorizontal + arrowWidth, y3 = settings.markerHeight;
  var markerArrowReactant = defs.append('marker').attr("id", uniquifyId("ceMarkerArrowReactant"));
  markerArrowReactant.append("line")
    .attr("x1", function () {
      return x2;
    })
    .attr("y1", function () {
      return y1;
    })
    .attr("x2", function () {
      return settings.markerWidth - settings.markerPaddingRight;
    })
    .attr("y2", function () {
      return y1;
    })
    .style("stroke", settings.markerColor)
    .style("stroke-width", linkStrokeWidth);
  for (let i = 1; i <= 3; i++) {
    markerArrowReactant.append("circle")
      .attr("cx", function () {
        return dotSpacing * i;
      })
      .attr("cy", centerVertical)
      .attr("r", function () {
        return dotR;
      })
      .style("fill", settings.markerColor);
  }
  markerArrowReactant.append("path")
    .attr("d",
      " M " + x1 + "," + y1 +
      " L " + x2 + "," + y2 +
      " L " + x3 + "," + y3 +
      " L " + x1 + "," + y1)
    .style("fill", settings.markerColor);

  /* VERTICAL */
  const xVertical = settings.markerWidth * 0.9;
  const markerVerticalProduct = defs.append('marker').attr("id", uniquifyId("ceMarkerVerticalProduct"));
  markerVerticalProduct.append("line")
    .attr("x1", 0)
    .attr("y1", centerVertical)
    .attr("x2", xVertical)
    .attr("y2", centerVertical)
    .style("stroke", settings.markerColor)
    .style("stroke-width", linkStrokeWidth);
  markerVerticalProduct.append("line")
    .attr("x1", xVertical)
    .attr("y1", 0)
    .attr("x2", xVertical)
    .attr("y2", settings.markerHeight)
    .style("stroke", settings.markerColor)
    .style("stroke-width", markerVerticalLineStrokeWidth);

  /* BACK VERTICAL */
  const markerVerticalReactant = defs.append('marker').attr("id", uniquifyId("ceMarkerVerticalReactant"));
  markerVerticalReactant.append("line")
    .attr("x1", centerHorizontal)
    .attr("y1", centerVertical)
    .attr("x2", function () {
      return settings.markerWidth - settings.markerPaddingRight;
    })
    .attr("y2", centerVertical)
    .style("stroke", settings.markerColor)
    .style("stroke-width", linkStrokeWidth);
  markerVerticalReactant.append("line")
    .attr("x1", centerHorizontal)
    .attr("y1", 0)
    .attr("x2", centerHorizontal)
    .attr("y2", settings.markerHeight)
    .style("stroke", settings.markerColor)
    .style("stroke-width", markerVerticalLineStrokeWidth);
  for (let i = 1; i <= 3; i++) {
    markerVerticalReactant.append("circle")
      .attr("cx", function () {
        return dotSpacing * i;
      })
      .attr("cy", centerVertical)
      .attr("r", function () {
        return dotR;
      })
      .style("fill", settings.markerColor);
  }

  /* CIRCLE */
  const cx = settings.markerWidth - settings.markerHeight / 2 - settings.markerPaddingRight;
  const cy = settings.markerHeight / 2;
  const markerCircle = defs.append('marker').attr("id", uniquifyId("ceMarkerCircle"));
  markerCircle.append("line")
    .attr("x1", 0)
    .attr("y1", function () {
      return cy;
    })
    .attr("x2", function () {
      return cx;
    })
    .attr("y2", function () {
      return cy;
    })
    .style("stroke", settings.markerColor)
    .style("stroke-width", linkStrokeWidth);
  markerCircle.append("circle")
    .attr("cx", function () {
      return cx;
    })
    .attr("cy", function () {
      return cy;
    })
    .attr("r", function () {
      return circleR;
    })
    .style("fill", settings.markerColor);

  /* ALL MARKERS */

  defs.selectAll("marker")
    .attr("markerWidth", function () {
      return settings.markerWidth;
    })
    .attr("markerHeight", function () {
      return settings.markerHeight;
    })
    .attr("refX", 0)
    .attr("refY", function () {
      return settings.markerHeight / 2;
    })
    .attr("orient", "auto")
    .attr("markerUnits", "userSpaceOnUse")
}

function getMouseClickPositionInSvg(event) {
  const offset = pluginContainer.find(".ceChart").offset();
  return {
    x: event.pageX - offset.left,
    y: event.pageY - offset.top
  }
}

function getSvgClassStrokeWidth(svgClass) {
  helper.assert(svgClass !== undefined);

  pluginContainer.find(".ceChart svg").append('<circle class="' + svgClass + '" id="circleForStrokeWidth"></circle>');
  let stroke = parseFloat(pluginContainer.find("#circleForStrokeWidth").css("stroke-width"));
  pluginContainer.find("#circleForStrokeWidth").remove();

  return stroke;
}

// ****************************************************************************
// ********************* MINERVA INTERACTION *********************
// ****************************************************************************

function initGlobals() {
  globals.models = {};
  return minervaWrapper.getModels().then(models => {
    helper.log("models", models);
    models.forEach((model, ix) => {
      globals.models[model.getId()] = model;
      if (ix === 0) globals.mainModelId = model.getId();
    })
  });
}

function createQuery(bioEntityId) {
  return {
    id: "_id" in bioEntityId ? parseInt(bioEntityId._id) : parseInt(bioEntityId.id),
    modelId: "_modelId" in bioEntityId ? bioEntityId._modelId : bioEntityId.modelId, //can happen when I pass results of search, i.e. the object of type IdentifiedElement
    type: "ALIAS"
  }
}

function matchSpeciesType(bioEntity) {
  let types = pluginContainer.find(".ceSelSpeciesType").val();
  return types.indexOf(getBETypeMapping(bioEntity.getType())) >= 0;
}

function fetchBioObject(bioEntityIds, type) {
  let queryArray = [];
  for (let i = 0; i < bioEntityIds.length; i++) {
    queryArray.push(createQuery(bioEntityIds[i]));
  }
  let promise;
  if (type === "ALIAS") promise = minervaWrapper.getBioEntityById(queryArray);
  else if (type === "REACTION") promise = minervaWrapper.getReactionsWithElement(queryArray);
  else helper.assert(false);

  return promise.then(function (results) {
    helper.log("results in fetchBioObject", results);
    return results.map(function (d) {
      return deepCopy(d);
    })
  }).catch(function (d) {
    helper.log("Caught error when fetching bioEntities", queryArray);
  });
}

function fetchReactions(bioEntityIds) {
  return fetchBioObject(bioEntityIds, "REACTION");
}

function fetchBioEntities(bioEntityIds) {
  return fetchBioObject(bioEntityIds, "ALIAS");
}

// ****************************************************************************
// ********************* EXPLORATION *********************
// ****************************************************************************

/**
 * Adds new root to the visualization.
 * @param {array<Reaction, MinervaElement>} entities Name of the element to be used as a root (can come either from search or from new root spawning action)
 * @param level The level where the tree should be inserted. All the trees in this and the following levels will be removed.
 */
function addRoot(entities, level = 0) {

  const query = typeof entities === "string" ? entities : entities[0].getName();
  return minervaWrapper.getElementsByQuery({query: query, perfectMatch: true}).then(function (result) {
    return fetchBioEntities(result);
  }).then(function (entities) {
    helper.log("entities", entities);

    helper.assert(typeof entities === "object" && entities.length > 0);

    removeRootsFrom(level);

    if (level === 0) {
      cleanLegends();
    }

    helper.log("d3 for entities", entities[0]);
    globals.treeRoots.push(d3.hierarchy({
      name: entities[0].getName(),
      _modelId: entities[0].getModelId(),
      children: [],
      type: entities[0].getType(),
      compartment: "",
      // reactionType: "",
      bioEntityIds: entities
    }));

    /*
     * d3Tree needs to be reinitialized each time in order to solve situations where nodes would not fit on screen.
     * In such a case, the height (or actually width before transformation) will be increased to fit
     * the number of nodes so that they do not overlap each other.
     */

    //Set new root position
    const height = getTreesMaxHeight(globals.treeRoots) - settings.marginTop - settings.marginBottom;
    globals.treeRoots[globals.treeRoots.length - 1].x0 =
      (globals.treeRoots.length === 1 ? height / 2 - settings.d3TreeHeightCorrection : globals.treeRoots[globals.treeRoots.length - 2].x0);
    globals.treeRoots[globals.treeRoots.length - 1].y0 = settings.nodeCircleSize / 2;

//        svg.selectAll("g.node").remove(); //TODO
    //Update needs to be done for all roots, since they might need to change color of their nodes, for example
    globals.treeRoots.forEach(function (root, i) {
      let highlight = (i === globals.treeRoots.length - 1);
      updateLayout(root, i, highlight);
    });

    if (globals.treeRoots.length === 1) {
      resetSvgPosition();
    } else {
      focusSvgOnPosition([getTreePosRight(globals.treeRoots.length - 2) - settings.treeLevelWidth, 0])
    }

    if (globals.treeRoots.length > 1) {
      findAndHighlightMatchingNodes(globals.treeRoots.length - 2);
    }

    //Adding of the first root adds the roots legend band at the top of the visualization which changes
    //the amount of space left for the chart
    recomputeChartDimensions();

    //Focus on the inserted root
    locateInMap(globals.treeRoots[globals.treeRoots.length - 1]);
  });
}

/**
 * Them main procedure which computes the nodes and edges positions and does the layouting.
 * @param source Which node was expanded (in case of clicking) or created (in case of spawning new root)
 * @param ixTree The tree which should be updated.
 * @param highlight Whether the corresponding entities should be highlighted in the map.
 */
function updateLayout(source, ixTree, highlight = true) {
  helper.log("tree root in updateLayout", source);

  // Compute the new tree layout.
  // var nodes = d3Tree[0].nodes(globals.treeRoots[ixTree]).reverse(),
  //     links = d3Tree[0].links(nodes);
  helper.log("trees max height", getTreesMaxHeight(globals.treeRoots));
  const d3Tree = d3.tree().size([getTreesMaxHeight(globals.treeRoots), 0]); //the max size can change with every change, so this needs to reinitialized every time. otherwise it could be computed just once

  d3Tree(globals.treeRoots[ixTree]);
  const links = globals.treeRoots[ixTree].links(); //tree adds x and y coordinates + results in object on which links function is called
  const nodes = globals.treeRoots[ixTree].descendants(); //gets all the objects which now have x and y coordinates

  // Normalize for fixed-depth. Sets Y (that is X after transformation) position for every node based on their level.
  nodes.forEach(function (d) {
    d.y = d.depth * settings.treeLevelWidth + settings.nodeCircleSize / 2;
  });

  let treeSvg = globals.svgG.select(".ceTree" + parseInt(ixTree));
  if (treeSvg.empty()) {
    treeSvg = globals.svgG.append("g")
      .attr("class", "ce-tree " + " ceTree" + parseInt(ixTree))
      .attr("transform", function () {
        return "translate(" + getTreePosLeft(ixTree) + ",0)"
      });
  }

  const node = treeSvg.selectAll("g.ce-node")
    .data(nodes, function (d) {
      return d.treeNodeId || (d.treeNodeId = ++globals.cnt_nodes);
    });

  // Enter any new nodes at the parent's previous position.
  const nodeEnter = node.enter().append("g")
    .attr("class", "ce-node")
    .attr("transform", function () {
      return "translate(" + source.y0 + "," + source.x0 + ")";
    })
    .on("mouseover", function (d) {
      if (pluginContainer.find(".ceChart").find("svg").attr("class") !== undefined &&
        pluginContainer.find(".ceChart").find("svg").attr("class").indexOf("ce-cursor-glass") >= 0) {

        locateInMap(d, true);
      }
    });

  nodeEnter
    .each(function (d, i) {

      let el = d3.select(this);
      let node_symbol = undefined;

      if (isNotCompartmentNodeData(d.data)) {
        node_symbol = el.append("circle").attr("r", 1e-6);
      } else {
        node_symbol = el.append("rect").attr("width", 1e-6).attr("height", 1e-6);
        let ncs2 = settings.nodeCircleSize / 2;
        if (d.depth == 1) {
          //if this is the submap information level, let's create a diamond symbol
          node_symbol.attr("transform", `translate(${ncs2}, ${ncs2}) rotate(45) translate(-${ncs2}, -${ncs2})`);
        }
      }

      node_symbol
        .on("click", nodeClickLeft)
        .on("contextmenu", nodeClickRight)
        .on('mouseover', function (d) {
          d3.select("#" + pluginContainerId + " .ceTooltip")
            .html(function () {
              let str;
              if (d.depth > 0 && isNotCompartmentNodeData(d.data)) {
                str = '<div class="ce-tooltip-species">' + d.data.name + "</div>";
                str += '<div class="ce-tooltip-bioentities">Bioentities:</div>';
                d.data.bioEntityIds.forEach(function (bioEntity) {
                  str +=
                    '<div class="ce-tooltip-bioentity">' +
                    '   <div>Id: <span class="ce-tooltip-bioentity-id">' + bioEntity.id + '</span></div>' +
                    '   <div>Role: <span class="ce-tooltip-bioentity-role">' + bioEntity.role + '</span></div>' +
                    '   <div>Raction type: <span class="ce-tooltip-bioentity-reaction-type">' + bioEntity.reactionType + '</span></div>';
                  if (isComplex(bioEntity)) {
                    //TODO - component decomposition
                  }

                  str += '</div>';
                });
              } else if (d.depth == 0) {
                str = `<div class="ce-tooltip-species">${d.data.name}</div>`;
              } else if (d.depth == 1) {
                /*
                The first level is the submap information.
                 */
                str = `<div class="ce-tooltip-species">(Sub)map ${d.data.name}</div>`;
              } else {
                const nameModelId = parseNodeNameInnerFormat(d.data.name);
                const modelName = globals.models[nameModelId.subMapId].getName();
                let nodeText = `${nameModelId.name} (map ${modelName})`;
                str = `<div class="ce-tooltip-species">${nodeText}</div>`;
              }
              return str;
            })
            .style("left", function () {
              return getMouseClickPositionInSvg(d3.event).x + 16 + "px";
            })
            .style("top", function () {
              return getMouseClickPositionInSvg(d3.event).y + "px";
            });
          d3.select("#" + pluginContainerId + " .ceTooltip").classed("hidden", false);
        })
        .on("mouseout", function () {
          d3.select("#" + pluginContainerId + " .ceTooltip").classed("hidden", true);
        });
    });

  nodeEnter.append("text")
    //            .attr("x", function(d) { return d.children || d._children ? -15 : 15; })
    .attr("x", function () {
      return settings.nodeTextOffsetX;
    })
    .attr("class", function (d) {
      let className = "ce-species";
      if (isNotCompartmentNodeData(d.data) && isComplex(d.data.bioEntityIds[0])) {
        className += " complex";
      }
      return className;
    })
    .attr("dy", ".35em")
    //            .attr("text-anchor", function(d) { return d.children || d._children ? "end" : "begin"; })
    .attr("text-anchor", "begin")
    //            .attr("text-color", "red")
    .html(function (d) {
      //return d.data.name.length > settings.maxNodeNameLength ? d.data.name.substring(0, settings.maxNodeNameLength-3)+"..." : d.data.name;
      // `<tspan x="${settings.nodeTextOffsetX}" dy="${modelComparmentTextOffsetY}">(map ${idModel})</tspan>`
      let name;
      if (isNotCompartmentNodeData(d.data) || d.depth == 1) {
        name = d.data.name;
      } else {
        const nameModelId = parseNodeNameInnerFormat(d.data.name);
        const modelName = globals.models[nameModelId.subMapId].getName();

        name = `${nameModelId.name}`;
        if (nameModelId.subMapId != globals.mainModelId) name += ` <tspan x="${settings.nodeTextOffsetX}" dy="${settings.modelComparmentTextOffsetY}">(${modelName})</tspan>`;
      }
      // return name;
      return name.length > settings.maxNodeNameLength ? name.substring(0, settings.maxNodeNameLength - 3) + "..." : name;
    })
    //            .attr("filter", "url(#solid)")
    .style("fill-opacity", 1e-6);


  // Transition nodes to their new position.
  const nodeUpdate = nodeEnter.merge(node);

  nodeUpdate
    .transition()
    .duration(settings.duration)
    .attr("transform", function (d) {

      let x = d.x;
      let y = d.y;

      if (!isNotCompartmentNodeData(d.data)) {
        x -= settings.nodeCircleSize / 2;
        y -= settings.nodeCircleSize / 2;
      }

      return "translate(" + y + "," + x + ")";
    })
    // .on("end")
    .call(transitionEnd, repositionTrees, ixTree + 1);
//            .call(transitionEnd, updateLinksToRoots);

  nodeUpdate.selectAll("circle")
    .attr("r", settings.nodeCircleSize / 2)
    .style("fill", function (d) {
      return getNodeColor(d, this);
    })
    .style("fill-opacity", function (d) {
      return "show" in d && !d.show ? 0.1 : 1;
    })
    .style("stroke", function (d) {
      return getColorForElement(this);
    })
    .style("stroke-opacity", function (d) {
      return "show" in d && !d.show ? 0.1 : 1;
    });

  nodeUpdate.selectAll("rect")
    .attr("width", settings.nodeCircleSize)
    .attr("height", settings.nodeCircleSize)
    .attr("rx", d => d.depth == 1 ? 0 : settings.nodeCircleSize / 10)
    .attr("ry", d => d.depth == 1 ? 0 : settings.nodeCircleSize / 10)
    .style("fill", settings.compartmentSymbolColor)
    .style("fill-opacity", function (d) {
      return "show" in d && !d.show ? 0.1 : 1;
    })
    .style("stroke", settings.compartmentSymbolColor)
    .style("stroke-opacity", function (d) {
      return "show" in d && !d.show ? 0.1 : 1;
    });

  nodeUpdate.select("text")
    .style("fill-opacity", 1);

  // Transition exiting nodes to the parent's new position.
  const nodeExit = node.exit().transition()
    .duration(settings.duration)
    .attr("transform", function (d) {
      return "translate(" + source.y + "," + source.x + ")";
    })
    .remove()
    .call(transitionEnd, function () {
      if (highlight) highlightInMap();
    });

  nodeExit.select("circle")
    .attr("r", 1e-6);

  nodeExit.select("rect")
    .attr("width", 1e-6)
    .attr("height", 1e-6);

  nodeExit.select("text")
    .style("fill-opacity", 1e-6);

  // Update the links…

  const diagonal = function (d) { //TODO d3.linkHorizontal()
    return "M" + d.source.y + "," + d.source.x
      + "C" + (d.source.y + d.target.y) / 2 + "," + d.source.x
      + " " + (d.source.y + d.target.y) / 2 + "," + d.target.x
      + " " + d.target.y + "," + d.target.x;
  };

  const link = treeSvg.selectAll("path.ce-link")
    .data(links, function (d) {
      return d.target.treeNodeId;
    });

  // Enter any new links at the parent's previous position.
  const linkEnter = link.enter().insert("path", "g")
    .attr("class", "ce-link")
    .attr("marker-end", function (d, i) {
      // if (!("role" in d.target)) return "";
      // else if (d.target.role.toLowerCase() == "product")
      //     return "url(#ceMarkerArrow)";
      // else if (d.target.role.toLowerCase() == "reactant")
      //     return "url(#ceMarkerVertical)";
      // else if (d.target.role.toLowerCase() == "modifier")
      //     return "url(#ceMarkerCircle)";
      // else return "";

      if (!("reactionType" in d.target.data)) return "";
      else {
        return "url(#" + getMarkerIdFromReaction(d.target.data.reactionType, d.target.data.role) + ")";
        // let markers = ["ceMarkerArrowReactant", "ceMarkerArrowProduct",
        //     "ceMarkerVerticalReactant", "ceMarkerVerticalProduct", "ceMarkerCircle"]
        // // return "url(#" + markers[Math.floor(Math.random() * markers.length)] + ")";
        // return "url(#" + markers[(i-1)%markers.length] + ")";
        //
        // reactionTypeDerived = getDerivedReactionType(d.target.data.reactionType).toLowerCase();
        // if (reactionTypeDerived  == "positive")
        //     return "url(#ceMarkerArrow)";
        // else if (reactionTypeDerived  == "negative")
        //     return "url(#ceMarkerVertical)";
        // else if (reactionTypeDerived  == "neutral")
        //     return "url(#ceMarkerVerticalReactant)";
        // else return "";
      }
    })
    .attr("d", function (d) {
      const o = {x: source.x0, y: source.y0};
      return diagonal({source: o, target: o});
    })
    .on("click", pathClick);

  // Transition links to their new position.
  const linkUpdate = linkEnter.merge(link);

  let nodeStrokeWidth = getSvgClassStrokeWidth("ce-node");
  linkUpdate.transition()
    .duration(settings.duration)
    .attr("d", function (d) {
      /*
      Change position of the curve end so that it ends before target. The offset size is defined by the
       marker space. Moving only the marker with refX and refY causes the marker not the follow the direction
       of the line for big node sizes
       */
      let y = d.target.y;
      if (isNotCompartmentNodeData(d.target.data)) {
        y -= (settings.nodeCircleSize / 2 + nodeStrokeWidth);
        y -= settings.markerWidth;
      }
      const target = {x: d.target.x, y: y};
      return diagonal({source: d.source, target: target});

    })
    .call(transitionEnd, backtrackPath);

  // Transition exiting links to the parent's new position.
  const linkExit = link.exit().transition()
    .duration(settings.duration)
    .attr("marker-end", "")
    .attr("d", function (d) {
      const o = {x: source.x, y: source.y};
      return diagonal({source: o, target: o});
    })
    .remove();

  // Stash the old positions for transition.
  nodes.forEach(function (x) {
    x.x0 = x.x;
    x.y0 = x.y;
  });

  updateLegends();
}

function isNotCompartmentNodeData(d) {
  return 'bioEntityIds' in d;
}

/**
 * Finds and highlights nodes which match (by name and modelId) with the following tree's root.
 * @param ixTree Index of the tree whose nodes should be checked for match with the following tree's root.
 */
function findAndHighlightMatchingNodes(ixTree) {
  helper.assert(ixTree >= 0);
  if (ixTree >= globals.treeRoots.length - 1) return;

  let node = globals.treeRoots[ixTree + 1];
  pluginContainer.find(".ceTree" + ixTree + " .ce-node").each(function () {
    let circle = pluginContainer.find(this).find("circle");
    const datum = d3.select(this).datum();
    if (isNotCompartmentNodeData(datum.data) && helper.objectsEquality(datum.data, node.data, ["name", "_modelId"])) {
      circle.css("stroke", "");
      d3.select(this).classed("ce-spawned", true);
    } else {
      d3.select(this).classed("ce-spawned", false);
      circle.css("stroke", function (d) {
        return getNodeColor(node, this);
      })
    }
  });
}

function removeRoot(ix) {
  pluginContainer.find(".ceTree" + ix).remove();
  globals.treeRoots.splice(ix, 1)
}

/**
 * Remove all the roots starting at given index.
 * @param ixRoot Index of the first root to be removed.
 */
function removeRootsFrom(ixRoot) {
  for (let ix = globals.treeRoots.length - 1; ix >= ixRoot; ix--) {
    removeRoot(ix);
    pluginContainer.find(".ceLegendTreeRootname" + ix).remove();
  }
}

function getMaxNumberOfNodesPerLevel(t) {
  let cntNodes = [];
  for (let i = 0; i <= t.height; i++) {
    cntNodes.push(0)
  }
  t.each(function (n) {
    cntNodes[n.depth]++;
  });
  return Math.max.apply(null, cntNodes);
}

/**
 * Retrieves maximum height of all the trees in list. This is used when deciding on the total height of the
 * visuzalizations.
 * @param tr List of trees to be considered.
 * @returns {number} Maximum of heights of individual trees.
 */
function getTreesMaxHeight(tr) {
  let maxCnts = [];
  for (let i = 0; i < tr.length; i++) {
    maxCnts.push(getMaxNumberOfNodesPerLevel(globals.treeRoots[i]));
  }
  let maxCnt = Math.max.apply(null, maxCnts);

  const br = pluginContainer.find(".ceChart svg")[0].getBoundingClientRect();
  return Math.max(
    br.height - settings.marginTop - settings.marginBottom - settings.d3TreeHeightCorrection,
    maxCnt * settings.nodeCircleSize + (maxCnt - 1) * settings.nodeSpacingVertical);
}

function getNodeColor(node, e) {
  return isExplorationEnd(node) ? "rgba(0,0,0, 0)" : getColorForElement(e);
}

function getBioentitiesWindow(bioEntities) {
  const coordsMin = {x: Number.MAX_VALUE, y: Number.MAX_VALUE};
  const coordsMax = {x: Number.MIN_VALUE, y: Number.MIN_VALUE};

  console.log(coordsMax, coordsMin);
  for (let i = 0; i < bioEntities.length; i++) {
    if (bioEntities[i].x < coordsMin.x) coordsMin.x = bioEntities[i].x;
    if (bioEntities[i].y < coordsMin.y) coordsMin.y = bioEntities[i].y;
    if (bioEntities[i].x > coordsMax.x) coordsMax.x = bioEntities[i].x;
    if (bioEntities[i].y > coordsMax.y) coordsMax.y = bioEntities[i].y;
    console.log(coordsMax, coordsMin);
  }

  return {x1: coordsMin.x, y1: coordsMin.y, x2: coordsMax.x, y2: coordsMax.y}
}

function locateInMap(node, subtree) {
  if (subtree === undefined) subtree = false;

  helper.assert(subtree || isNotCompartmentNodeData(node.data));

  function getBioentitiesUpToLeaves(node) {
    let be = [];
    if (isNotCompartmentNodeData(node.data)) be = node.data.bioEntityIds;
    if (nodeHasVisibleChildren(node.data)) {
      for (let i = 0; i < node.children.length; i++) {
        be = be.concat(getBioentitiesUpToLeaves(node.children[i]))
      }
    }
    return be;
  }

  let bioEntityIds = subtree ? getBioentitiesUpToLeaves(node) : node.data.bioEntityIds;

  if (bioEntityIds.length > 0) {
    const coords = getBioentitiesWindow(bioEntityIds);

    let modelId = bioEntityIds[0]._modelId;

    return minervaWrapper.openMap({id: modelId}).then(function () {
      return minervaWrapper.fitBounds({
        modelId: modelId,
        x1: coords.x1,
        y1: coords.y1,
        x2: coords.x2,
        y2: coords.y2
      });

    });
  } else {
    return Promise.resolve();
  }
}

function getColorForTreeIx(ix) {
  return globals.color20c(globals.treeRoots.length - 1 - ix); //the color of the last tree will be always the same

}

function getColorForElement(e) {
  return getColorForTreeIx(getParentTreeIx(e));
}

function highlightPath(el) {
  let node = el.__data__;
  // const ixTree = getParentTreeIx(el);

  let path = [];
  while ("parent" in node) {
    path = path.concat(node);
    node = node.parent;
  }
  path = path.concat(node);
}

function deHighlightAll() {
  return minervaWrapper.getHighlightedBioEntities().then(highlighted => {
    return minervaWrapper.hideBioEntity(highlighted.map(function (entry) {
      return {
        element: {
          type: isReaction(entry.element) ? "REACTION" : "ALIAS"
        },
        bioEntity: entry.element,
      }
    }));
  });
}

function isReaction(bioEntity) {
  return !("_compartmentId" in bioEntity);
}

function highlightInMap() {
  helper.log("highlight in map: ");

  return minervaWrapper.getHighlightedBioEntities().then(function (highlighted) {
    helper.log("highlighted: ", highlighted);
    let highligtedIds = highlighted.map(function (d) {
      let type = isReaction(d.element) ? "REACTION" : "ALIAS";
      return {
        element: {id: d.element.getId(), modelId: d.element.getModelId(), type: type},
        type: d.type,
        options: deepCopy(d.options),
        bioEntity: d.element
      }
    });
    //remove duplicates
    highligtedIds = highligtedIds.filter(function (item, pos, self) {
      return helper.objectInArray(item, self, ['element', 'options', 'type']) === pos;
    })


    const toShow = [];
    const toKeep = [];
    //If a path is present in the tree, only that will be highlighted
    if (pluginContainer.find(".ce-path-link").length > 0) {
      helper.log("path highlight");
      pluginContainer.find(".ce-path-link").each(function () {
        let d = getD3Datum(this);
        if (isNotCompartmentNodeData(d.target.data)) { //by highlighting only targets, we avoid highlighting all the root entities
          for (let i = 0; i < d.target.data.bioEntityIds.length; i++) {
            let dd = d.target.data.bioEntityIds[i];
            let e = {
              element: {id: dd.id, modelId: dd._modelId, type: "ALIAS"},
              type: "ICON"
            };
            const ix = helper.objectInArray(e, highligtedIds, ['element', 'options', 'type']);
            if (ix < 0) toShow.push(e); else toKeep.push(ix);

            if ("reaction" in dd) {
              let e = {
                element: {id: dd.reaction.id, modelId: dd._modelId, type: "REACTION"},
                type: "SURFACE",
                options: {
                  color: "#cc0000",
                }
              };
              const ix1 = helper.objectInArray(e, highligtedIds, ['element', 'options', 'type']);
              helper.log("is reaction to highlight in highlighted ids?", e, highligtedIds, ix1);
              if (ix1 < 0) toShow.push(e); else toKeep.push(ix1);
            }
          }
        }
      });
    } else {

      pluginContainer.find(".ce-node").each(function () {
        let d = getD3Datum(this);
        if (isNotCompartmentNodeData(d.data)) {
          // ceSpecies.push(d);
          for (let i = 0; i < d.data.bioEntityIds.length; i++) {
            let dd = d.data.bioEntityIds[i];
            let e = {
              element: {id: dd.getId(), modelId: dd._modelId, type: "ALIAS"},
              // type: settings.neighboursBySurface || d.depth == 0 ? "SURFACE" : "ICON",
              options: {
                color: getColorForTreeIx(globals.treeRoots.length - 1),
                opacity: settings.highlightedNodeOpacity,
                lineColor: getColorForTreeIx(globals.treeRoots.length - 1),
                lineWeight: settings.highlightedNodeBorder,
                lineOpacity: settings.highlightedNodeBorderOpacity
                // color: getColorForElement($(this)[0])
              },
              bioEntity: dd
            };
            if (settings.neighboursBySurface) {
              e.type = (d.depth === 0 ? "ICON" : "SURFACE");
            } else {
              e.type = (d.depth === 0 ? "SURFACE" : "ICON");
            }
            const ix = helper.objectInArray(e, highligtedIds, ['element', 'options', 'type']);
            if (ix < 0) toShow.push(e); else toKeep.push(ix);

            if ("reaction" in dd) {
              let e = {
                element: {id: dd.reaction.id, modelId: dd._modelId, type: "REACTION"},
                type: "SURFACE",
                options: {
                  // color: getColorForElement($(this)[0])
                  opacity: settings.highlightedNodeOpacity,
                  color: getColorForTreeIx(globals.treeRoots.length - 1)
                },
                bioEntity: dd.reaction
              };
              const ix1 = helper.objectInArray(e, highligtedIds, ['element', 'options', 'type'])
              if (ix1 < 0) toShow.push(e); else toKeep.push(ix1);
            }
          }
        }
      });
    }

    const toHide = [];
    for (let i = 0; i < highligtedIds.length; i++) {
      if (toKeep.indexOf(i) < 0 && helper.objectInArray(highligtedIds[i], toShow, ['element', 'options', 'type']) < 0) {
        toHide.push(highligtedIds[i]);
      }
    }

    return minervaWrapper.hideBioEntity(toHide).then(function (d) {
      return minervaWrapper.showBioEntity(toShow);
    });
  });

}

function isExplorationEnd(node) {
  return /*node.depth > 0 && */ "isLeaf" in node; //TODO
  // return !(node.depth == 0 ||
  // (Object.keys(node).indexOf("children") >= 0 && node.children.length > 0) ||
  // (Object.keys(node).indexOf("_children") >= 0 && node._children.length > 0));
}

function transitionEnd(transition, callback, attr) {
  let n;
  if (transition.empty()) {
    callback(attr);
  } else {
    n = transition.size();
    transition.on("end", function () {
      n--;
      if (n === 0) {
        callback(attr);
      }
    });
  }
}

function getParentTreeIx(e) {
  //the class of the group containes something like "ce-tree ceTree3", and we need the index
  return parseInt(/ceTree([0-9])*/.exec($(e).parents("g.ce-tree").attr("class"))[1]);
}

function pathClick(path) {
  // menu = d3.select("#" + pluginContainerId + " .cePathMenu");
  //
  //
  // if (d3.select(this).classed(".ce-path-link")) {

  //     menu = d3.select(pluginContainerId + " .cePathMenu");
  //     menu.selectAll("div").remove();
  //     menu.append("div")
  //         .text("CLEAR PATH")
  //         .style("font-size", 10)
  //         .on("click", function(d){
  //             d3.select(pluginContainerId + " .cePathMenu").classed("hidden", true);
  //             clearPaths();
  //         })
  //         .style("left", d3.event.pageX + 16 + "px")
  //         .style("top", d3.event.pageY + "px")
  //         .classed("hidden", false);
  // }

}

function nodeClickLeft(node) {
  nodeMenuClick('exp', this.parentNode);
}

function nodeClickRight(node) {

  d3.event.preventDefault();

  let parentG = this.parentNode;

  let linkTexts = [];
  const fullExpansionDef = [`RECURSIVE EXPLORATION (${pluginContainer.find('input[name="ceFullExpansionMaxCount"]').val()} nodes limit)`, "full_exp"];
  if (!isNotCompartmentNodeData(node.data)) {
    linkTexts = [/*["EXPAND/COLAPSE", "exp"],*/ ["LOCATE SUBTREE", "locate_subtree"], fullExpansionDef];
  } else {
    if (node.depth === 0) linkTexts = [/*["EXPAND/COLLAPSE", "exp"],*/ ["REMOVE ROOT", "despawn"], ["LOCATE", "locate"], ["LOCATE SUBTREE", "locate_subtree"], fullExpansionDef];
    else if (isExplorationEnd(node)) linkTexts = [["LOCATE", "locate"], ["SPAWN NEW ROOT", "spawn"], fullExpansionDef];
    else linkTexts = [/*["EXPAND/COLLAPSE", "exp"],*/ ["SPAWN NEW ROOT", "spawn"], ["LOCATE", "locate"], ["LOCATE SUBTREE", "locate_subtree"], fullExpansionDef];
    // linkTexts = linkTexts.concat([["HIGHLIGHT PATH", "highlight_path"]])

    if (node.depth > 0) {
      linkTexts = node.data.getId() === globals.lastBacktrackNodeId ? linkTexts.concat([["CLEAR BACKTRACK", "clear_backtrack"]]) : linkTexts.concat([["BACKTRACK", "backtrack"]]);
    }
  }

  const selMenu = d3.select("#" + pluginContainerId + " .ceNodeMenu");
  if (selMenu.classed("hidden")) {
    selMenu.selectAll("div").remove();
    selMenu.selectAll("div").data(linkTexts)
      .enter()
      .append("div")
      .text(function (d) {
        return d[0];
      })
      .style("font-size", 10)
      .on("click", function (d) {
        nodeMenuClick(d[1], parentG);
      });
    selMenu
      .style("left", function () {
        return getMouseClickPositionInSvg(d3.event).x + 16 + "px";
      })
      .style("top", function () {
        return getMouseClickPositionInSvg(d3.event).y + "px";
      })
      .classed("hidden", false);
  } else selMenu.classed("hidden", true);

  return false;
}

/*
 * If bioentities match in keys, they are merged into one entity. The information about the
 * individual entities comprising the merged entity is stored in the bioEntity variable.
 */
function groupBioEntites(entities, keys) {
  for (let i = entities.length - 1; i >= 0; i--) {
    for (let j = i - 1; j >= 0; j--) {
      if (helper.objectsEquality(entities[i], entities[j], keys, false)) {
        entities[j].bioEntityIds = entities[j].bioEntityIds.concat(entities[i].bioEntityIds);
        entities.splice(i, 1);
        break;
      }
    }
  }
}

/*
 If any of the entities appears in rootPath (with respect to given keys), it will be removed from the list.
 */
function filterBioEntites(entities, rootPath, keys) {
  for (let i = entities.length - 1; i >= 0; i--) {
    for (let j = 0; j < rootPath.length; j++) {
      let match = true;
      for (let k = 0; k < keys.length; k++) {
        //If key is not in the rootPath object, is is considered as matched (happens in roots which have only type and name)
        if (Object.keys(rootPath[j]).indexOf(keys[k]) >= 0 && entities[i][keys[k]] !== rootPath[j][keys[k]]) {
          match = false;
          break;
        }
      }
      if (match) {
        entities.splice(i, 1);
        break;
      }
    }
  }
}

function fetchNeighbours(node) {
  helper.log("fetchNeighbors of node.data.bioeEntityIds", node.data.bioEntityIds);
  return fetchReactions(node.data.bioEntityIds).then(function (bioEntitiesReactions) {
    helper.log("bioEntitiesReactions", deepCopy(bioEntitiesReactions));
    let bioEntities = [];
    for (let i = 0; i < bioEntitiesReactions.length; i++) {
      const reaction = bioEntitiesReactions[i];

      ["products", "reactants", "modifiers"].forEach(function (typeName) {
        let ents = reaction["_" + typeName].map(function (d) {
          let e = d.getAlias();
          e.role = typeName.slice(0, -1);
          e.reactionType = reaction._type;
          e.reaction = reaction;
          return e;
        });
        // Remove the products, reactants and modifiers to get rid of circular references which are problematic
        // later when copying objects with JSON.stringify. We are working here over copies of the original
        // reactions objects, so this does not affect the reactions in the map.
        delete reaction["_" + typeName];
        bioEntities = bioEntities.concat(ents);
      });
    }

    //Filter out bioentities not matching selected species type
    helper.log("bioEntities before filtration by type", bioEntities);
    bioEntities = bioEntities.filter(matchSpeciesType);
    helper.log("bioEntities after filtration by type", bioEntities);

    helper.uniquify(bioEntities, ["id", "role"]);
    helper.log("uniquified", bioEntities);

    let compartmentIds = bioEntities.map(function (d) {
      return {
        "id": d.getCompartmentId(),
        "_modelId": d.getModelId()
      }
    });
    //remove records without _compartmentId
    compartmentIds = compartmentIds.filter(function (d) {
      return d.id !== undefined && d.id !== null;
    });
    helper.log("compartmentIds without nulls", compartmentIds);
    helper.uniquify(compartmentIds, ["id", "_modelId"]);
    helper.log("after uniq compartmentIds", compartmentIds);

    //For each compartment and model we have one representative bioentity
    return fetchBioEntities(compartmentIds).then(function (compartments) {
      helper.log("fetched compartments", compartments);
      //Convert compartment into a dictionary
      const dictCompartments = {};
      compartments.forEach(function (d) {
        dictCompartments[d.getId()] = d;
      });
      helper.log("dictCompartments", dictCompartments);

      helper.log("bioents to connect to compartments", bioEntities);
      //Attach compartment names to the original bioentities, remove reactants and products and return them
      bioEntities.forEach(function (d) {
        let idAux = d.getCompartmentId();
        d.comparmentName = idAux ? dictCompartments[idAux].name : settings.emptyCompartmentName;
      });
      helper.log("bioents with compartments", bioEntities);
      return bioEntities;
    });
  });
}

function getMarkerIdFromReaction(reactionName, role) {
  role = role.toLowerCase();
  if (role == 'modifier') return uniquifyId('ceMarkerCircle');
  else {
    if (
      ['inhibition',
        'unknown inhibition',
        'negative influence',
        'unknown negative influence'
      ].indexOf(reactionName.toLowerCase()) >= 0) {

      return uniquifyId(role === 'reactant' ? 'ceMarkerVerticalReactant' : 'ceMarkerVerticalProduct');
    } else return uniquifyId(role === 'reactant' ? 'ceMarkerArrowReactant' : 'ceMarkerArrowProduct');
  }

  return '';
}

/**
 * Concats compartment name with model Id and uses this
 * to d3.nest the nodes. Adding model id helps to
 * differentiate compartments in different maps.
 * @param d
 * @returns {string}
 */
function convertCompartmentToInnerFormatName(d) {
  return d.comparmentName + '-' + d.getModelId();
}

function parseNodeNameInnerFormat(nodeName) {

  const match = nodeName.match(/(.*)-([^-]*)/);
  if (match) {
    return {
      name: match[1],
      subMapId: match[2]
    }
  } else {
    return {
      name: nodeName,
      subMapId: null
    }
  }
}

function fetchChildren(node) {

  let isRoot = node.depth === 0;

  return fetchNeighbours(node).then(function (bioEntities) {
    bioEntities.forEach(function (d, i) {
      bioEntities[i].bioEntityIds = [deepCopy(d)];
    });

    /*
    Group entities, e.g. when there are multiple occurrences of a species in the same compartment with
    the same role, complex or whatever grouping conditions are specified.
     */
    groupBioEntites(bioEntities, ["name", "_compartmentId", "_complexId", "role", "reactionType"]);

    /*
     Filter out entities which have already been encountered.
     */
    //Get object on path to root
    const rootPath = [node.data];
    let nodeAux = node;
    while (nodeAux.parent) {
      nodeAux = nodeAux.parent;
      rootPath.push(nodeAux.data);
    }
    filterBioEntites(bioEntities, rootPath, PATH_EQUALITY_ATTRS);
    helper.log("filtered bioentities", deepCopy(bioEntities));

    helper.log("bioEntities before nesting", deepCopy(bioEntities));
    //Nesting results in deepcopy, so any modifications are made to copies of the map objects
    let nested;
    if (!isRoot) {
      nested = d3.nest()
        .key(d => convertCompartmentToInnerFormatName(d)).sortKeys(d3.ascending)
        .sortValues((a, b) => a.getName() > b.getName())
        .entries(bioEntities);
    } else {
      nested = d3.nest()
        .key(d => globals.models[d.getModelId()].getName()).sortKeys(d3.ascending)
        .key(d => convertCompartmentToInnerFormatName(d)).sortKeys(d3.ascending)
        .sortValues((a, b) => a.getName() > b.getName())
        .entries(bioEntities);
    }


    helper.log("nested", deepCopy(nested));

    //Rename the attributes key and values to name and children which is what d3.hieararchy, and mainly the rest
    //of the application, is using (historical reasons).
    function renameNestAttrs(nest) {
      if ("values" in nest) {
        helper.renameProperty(nest, "key", "name");
        helper.renameProperty(nest, "values", "children");
        for (let i = 0; i < nest.children.length; i++) renameNestAttrs(nest.children[i])
      }
    }

    nested.forEach(function (n) {
      renameNestAttrs(n);
    });

    helper.log("renamed nested", deepCopy(nested));


    function modifyLeafs(x, depth) {
      if (depth === undefined) depth = 0;

      if ("children" in x) {
        for (let i = 0; i < x.children.length; i++) {
          modifyLeafs(x.children[i], depth + 1)
        }
      } else { //TODO
        // x.name = x.w;
//                        x.name = getNodeNameByGroupByIx(x, ixLastNotNullGroupBy);
        x.type = x.getType();
        x.compartment = x.getCompartmentId();
        x.complex1 = node.data.bioEntityIds[0].getComplexId();
        x.complex2 = x.getComplexId();
      }
    }

    for (let i = 0; i < nested.length; i++) {
      modifyLeafs(nested[i]);
      //convert to d3. hierarchy
      nested[i] = d3.hierarchy(nested[i])
    }

    // helper.log("nested with modified leafs", deepCopy(nested));

    return nested;
  });
}

/*
Called when children of a node were modified -> depth and height of the D3 hierarchy could have changed
 */
function updateHierarchy(node) {

  node.depth = node.parent ? node.parent.depth + 1 : 0;
  let count = 0;
  if (node.children) {
    node.children.forEach(n => {
      count += updateHierarchy(n);
    });
    const heights = node.children.map(function (d) {
      return d.height;
    });
    node.height = Math.max.apply(null, heights) + 1;

    // //In case new children were added, the pointers to children data objects need to be updated
    // node.data.children = node.children.map(function(d) {return d.data} );
  } else {
    node.height = 0;
    count += 1;
  }

  return count;
}

function getLeaves(node) {
  let leafs = [];
  if (node.children === undefined || node.children.length === 0) {
    leafs = [node];
  } else {
    node.children.forEach(n => {
      leafs = leafs.concat(getLeaves(n));
    })
  }
  return leafs;
}

function expandNodeFull(currentLeaves, ixRoot, newNodes) {
  let maxLeafs = pluginContainer.find('input[name="ceFullExpansionMaxCount"]').val();
  newNodes = newNodes.concat(currentLeaves);
  if (newNodes.length < maxLeafs && currentLeaves.length > 0) {
    const promises = currentLeaves.map(l => expandNode(l, ixRoot));
    return Promise.all(promises).then(rvs => {
      let newLeaves = [];
      rvs.forEach(rv => {
        newLeaves = newLeaves.concat(rv);
      });
      return expandNodeFull(newLeaves, ixRoot, newNodes);
    });
  } else {
    return newNodes;
  }
}

function expandNode(node, ixRoot) {
  return new Promise(function (resolve) {

    if (node.children) {
      node._children = node.children;
      delete node.children;
      // node.children = null;
      resolve(false);
    } else if (node._children) {
      node.children = node._children;
      /* Since every leaf since level 1 has _children, we have to call expansion in this
      branch to explore whether the leafs of the just expanded node (which might have been a
      leaf node) still have children.
       */
      // Promise.all(expandNextLevel(node, fetchChildren)).then(function(){resolve();});
      resolve(false);
    } else {
      fetchChildren(node).then(function (ch) {
        //parent of the new children is null, so it needs to be updated
        let isLeaf = !(ch && ch.length > 0);
        if (!isLeaf) {
          ch.forEach(function (d) {
            d.parent = node;
          });
          node.children = ch;
        }

        /*
         We need also next level in order to be able to visualize whether that is the end of the tree or not.
         This needs to be done for leaves only since the non-leaves do not need to correspond to proteins.
         */
        // Promise.all(expandNextLevel(node, fetchChildren)).then(function(){resolve();});
        resolve(isLeaf);
      });
    }
  }).then(function (isLeaf) {
    if (isLeaf) node.isLeaf = true;
    updateHierarchy(globals.treeRoots[ixRoot]);
    return isLeaf ? [] : getLeaves(node);
  });

}

function nodeMenuClick(code, el) {
  const node = d3.select(el).datum();
  const ixRoot = getParentTreeIx(el);
  new Promise(function (resolve) {
    if (code !== "exp" && code !== 'full_exp') {
      if (code === "spawn") {
        if (isNotCompartmentNodeData(node.data)) addRoot(node.data.name, ixRoot + 1); //no root path => infinite spawning
      } else if (code === "locate") {
        locateInMap(node);
      } else if (code === "locate_subtree") {
        locateInMap(node, true);
      } else if (code === "highlight_path") {
        highlightPath(el);
      } else if (code === "backtrack") {
        backtrackPath(el);
      } else if (code === "clear_backtrack") {
        clearPaths();
        highlightInMap();
      } else if (code === "despawn") {
        if (node.depth === 0) {
          removeRootsFrom(ixRoot);
          highlightInMap();
        }
      }
      resolve();
    } else if (code === 'exp') {
      return expandNode(node, ixRoot).then(() => {
        updateLayout(node, ixRoot);
        if (pluginContainer.find("#ceLocateOnExpansion").is(":checked")) locateInMap(node, true);
        return Promise.resolve();
      });
    } else if (code === 'full_exp') {
      resolve(expandNodeFull([node], ixRoot, []).then(() => {
        updateLayout(node, ixRoot);
        if (pluginContainer.find("#ceLocateOnExpansion").is(":checked")) locateInMap(node, true);
        return Promise.resolve();

      }))
    }
  }).then(function () {
    findAndHighlightMatchingNodes(ixRoot);
    d3.select("#" + pluginContainerId + " .ceNodeMenu").classed("hidden", true);
    // removeNodeMenu();
  });
}

function clearPaths() {
  pluginContainer.find(".ce-path-link").each(function () {
    d3.select(this).classed("ce-path-link", false);
  })
  globals.lastBacktrackEl = undefined;
  delete globals.lastBacktrackNodeId;
}

/*
 For starting node finds path in the respective tree to its root and then iteratively finds nodes in tree i-1
 which spawned root of i and for each of the finds path to the root. For each of the nodes on the path,
 it sets class for the link element in the visualization.
 */
function backtrackPath(el) {
  if (el === undefined) el = globals.lastBacktrackEl;
  else globals.lastBacktrackEl = el;

  if (el === undefined) return;

  clearPaths();

  if ($(el).length > 0) {
    //the original path start was not removed (hidden)

    globals.lastBacktrackNodeId = getD3Datum(el).data.getId();

    let maxDepth = pluginContainer.find("#ceBacktrackMaxDepth").val();
    if (maxDepth == "") maxDepth = Number.MAX_VALUE;

    helper.log("maxDepth", maxDepth);

    const ixTree = getParentTreeIx(el);
    for (let i = ixTree; i >= Math.max(ixTree - maxDepth + 1, 0); i--) { //for each tree
      let startNodeElements = [];
      if (i === ixTree) {
        startNodeElements = [el];
      } else { //find trees which spawned root
        startNodeElements = pluginContainer.find(".ceTree" + i + " .ce-spawned").toArray();
      }

      let treeLinks = pluginContainer.find(".ceTree" + i + " .ce-link");

      startNodeElements.forEach(function (startNodeElement) {
        let path = getD3Datum(startNodeElement).path(globals.treeRoots[i]);
        for (let j = 0; j < path.length - 1; j++) {
          treeLinks.each(function () {
            let linkDatum = getD3Datum(this);
            if (linkDatum.target === path[j] && linkDatum.source === path[j + 1]) {
              d3.select(this).classed("ce-path-link", true);
            }
          })
        }
      })
    }
  }

  highlightInMap();
}

function getD3Datum(e) {
  return e.__data__;
}

function nodeHasVisibleChildren(node) {
  return ("children" in node && node["children"].length > 0);
}

// function nodeHasChildren(node) {
//     return (("_children" in node && node["_children"].length > 0) || nodeHasVisibleChildren(node));
// }

// function removeNodeMenu() {
//     d3.selectAll("#" + pluginContainerId + " g.ce-node circle")
//         .attr("r", function () {
//             _r = d3.select(this).attr("_r");
//             if (_r != null) return _r;
//             else return d3.select(this).attr("r");
//         });
//     d3.selectAll("#" + pluginContainerId + " g.ce-node text")
//         .attr("text-anchor", function () {
//             _ta = d3.select(this).attr("_textAnchor");
//             if (_ta != null) return _ta;
//             else return d3.select(this).attr("text-anchor");
//         });
//     d3.selectAll("#" + pluginContainerId + " .ce-node-menu").remove();
// }

function repositionTrees(ixTree) {
  const cntTrees = pluginContainer.find(".ce-tree").length;
  if (ixTree >= cntTrees) return;

  const dist = getTreePosRight(ixTree - 1) - getTreePosLeft(ixTree);

  for (let ix = ixTree; ix < cntTrees; ix++) {
    const newLeft = getTreePosLeft(ix) + dist;
    d3.select("#" + pluginContainerId + " .ceTree" + parseInt(ix))
      .transition()
      .duration(settings.duration)
      .attr("transform", "translate(" + parseInt(newLeft) + ",0)");
  }
}

function cleanLegends() {
  d3.select("#" + pluginContainerId + " #ceLegendCompartmentsColors").selectAll("div").remove();
  d3.select("#" + pluginContainerId + " #ceLegendReactionsColors").selectAll("div").remove();
}

function focusOnRoot(ixRoot) {
  focusSvgOnPosition([getTreePosLeft(ixRoot), 0]);
  locateInMap(globals.treeRoots[ixRoot]);
}

function updateLegends() {
  let divLegend = pluginContainer.find(".ceLegend");
  divLegend.empty();
  let divLegendTrees = $('<div class="ce-legend-trees"></div>').appendTo(divLegend);
  let divLegendTreesRow = $('<div class="ce-legend-trees-row"></div>').appendTo(divLegendTrees);
  for (let i = 0; i < globals.treeRoots.length; i++) {
    let divTree = $("<div></div>").appendTo(divLegendTreesRow);
    divTree.attr("class", function () {
      return "ce-legend-tree-label ce-legend-tree-cell ceLegendTreeRootname" + i;
    });
    divTree.css("background-color", function () {
      return getColorForTreeIx(i);
    });
    divTree.text(function () {
      return globals.treeRoots[i].data.name;
    });
    divTree.on("click", function () {
      focusOnRoot(i);
    });
  }
}

function zoomTransformX(pos) {
  helper.log("zoomTransformX");
  return d3.zoomTransform(pluginContainer.find(".ceSvg > g")[0]).applyX(pos);
}

function zoomScale(x) {
  return x * d3.zoomTransform(pluginContainer.find(".ceSvg > g")[0]).k;
}

/*
 Returns tree right coordinate relative to the SVG element, including the zoom transformation.
 */
function getTreePosRight(ixTree) {
  const tree = pluginContainer.find(".ceTree" + parseInt(ixTree));
  const pos = tree.length ? getTreePosLeft(ixTree) + tree[0].getBBox().width : 0;
  helper.assert(typeof pos === "number");
  return pos;
}

/*
    Returns tree left coordinate relative to the SVG element, including the zoom transformation.
     */
function getTreePosLeft(ixTree) {
  let pos = 0;
  for (let i = 0; i < ixTree; i++) pos += pluginContainer.find(".ceTree" + parseInt(i))[0].getBBox().width + settings.treeMarginRight;
  return pos;
}

function resetSvgPosition(redraw) {
  if (redraw === undefined) redraw = true;
  d3.select("#" + pluginContainerId + " .ceSvg > g").call(globals.zoom.transform, d3.zoomIdentity); //for some reason when using globals.svgG, instead of the d3 selection, the transform of the g element does not get updated!
}

function focusSvgOnPosition(coords) {
  d3.select("#" + pluginContainerId + " .ceSvg > g").transition().duration(settings.duration)
    .call(globals.zoom.transform, d3.zoomIdentity.translate(-coords[0], -coords[1]));
}

function closeModal() {
  pluginContainer.find("#ceModal").modal("hide");
}

function openModal(params) {

  let modal = pluginContainer.find("#ceModal");
  let modalContent = modal.find(".modal-content");
  modalContent.empty();
  let modalContentClass = "modal-content";
  if ("modalClass" in params) modalContentClass += " " + params.modalClass;
  modalContent.attr("class", modalContentClass);
  if ("header" in params) {
    let header = pluginContainer.find('<div class="modal-header panel-heading"></div>').appendTo(modalContent);
    header.append('<button type="button" class="close">&times;</button>');
    header.append('<h4>' + params.header + '</h4>');

  }
  if ("body" in params) {
    let body = $('<div class="modal-body"></div>').appendTo(modalContent);
    body.append(params.body);
  }

  modal.modal("show");
}

const exportToFormat = function (format) {
  let exportString;
  if (format === "csv") exportString = exportTrees2CSV();
  if (format === "json") exportString = exportTrees2JSON();

  let fileName = 'export.' + format;
  let blob = new Blob([exportString], {type: 'text/' + format + ';charset=utf-8;'});
  if (navigator.msSaveBlob) { // IE 10+
    navigator.msSaveBlob(blob, fileName);
  } else {
    let link = document.createElement("a");
    if (link.download !== undefined) { // feature detection
      // Browsers that support HTML5 download attribute
      const url = URL.createObjectURL(blob);
      link.setAttribute("href", url);
      link.setAttribute("download", fileName);
      link.style.visibility = 'hidden';
      document.body.appendChild(link);
      link.click();
      document.body.removeChild(link);
    }
  }

  closeModal();
};

function exportTrees2JSON() {
  let trees = [];
  for (let i = 0; i < globals.treeRoots.length; i++) {
    trees.push(exportTree2JSON(globals.treeRoots[i]));
  }
  return JSON.stringify(trees, null, 2);
}

function exportTree2JSON(node) {
  const rv = {};
  rv.name = node.data.name;

  if (isNotCompartmentNodeData(node.data)) {
    let bioEntity = node.data.bioEntityIds[0];
    rv.element_type = bioEntity.getType();
    if ("reaction" in bioEntity) {
      rv.reaction_type = bioEntity.reaction.getType();
      rv.role_in_reaction = bioEntity.role;
    }
  } else {
    rv.name = parseNodeNameInnerFormat(rv.name).name;
    rv.element_type = node.depth == 1 ? settings.submapTypeName : settings.compartmentTypeName;
  }
  if ("children" in node) {
    rv.children = [];
    for (let i = 0; i < node.children.length; i++) {
      rv.children.push(exportTree2JSON(node.children[i]));
    }
  }

  return rv;
}

function exportTrees2CSV() {
  let csvString = "";
  for (let i = 0; i < globals.treeRoots.length; i++) {
    csvString += exportTree2CSV(globals.treeRoots[i]);
    if (i < globals.treeRoots.length - 1) csvString += "\n\n";
  }
  return csvString;
}

function exportTree2CSV(node, rows, row) {
  if (rows === undefined) rows = [];
  if (row === undefined) row = [];

  if (isNotCompartmentNodeData(node.data)) {
    let nodeString = node.data.name;
    let bioEntity = node.data.bioEntityIds[0];
    nodeString += "[" + bioEntity.getType() + "]";
    if ("reaction" in bioEntity) {
      row.push(bioEntity.reaction.getType());
      nodeString += "[" + parseNodeNameInnerFormat(node.parent.data.name).name + "]"; //compartment name
      nodeString += "[" + bioEntity.role + "]";
    }
    row.push(nodeString);
  } else if (node.depth == 1) {
    //submap
    row.push(node.data.name);
  }

  if (node.children) {
    for (let i = 0; i < node.children.length; i++) {
      exportTree2CSV(node.children[i], rows, deepCopy(row));
    }
  }

  if (node.height === 0) {
    rows.push(row);
  }
  if (node.depth === 0) {
    return rows.join('\n');
  }
}

//Dragging and zooming behavior
function zoomed() {
  globals.svgG.attr("transform", d3.event.transform);
}

function isComplex(bioEntity) {
  return bioEntity.getType().toLowerCase() === "complex";
}
